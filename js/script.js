let num1 = +prompt('Enter first number', '1');
while (!num1 || !isFinite(num1)) {
    num1 = +prompt('Number is expected, please re-enter');
}

let sign = prompt('Enter math sign, please', '+');
while (sign !== '+' && sign !== '-' && sign !== '*' && sign !== '/') {
    sign = prompt('Math sign is expected, Please re-enter');
}

let num2 = +prompt('Enter second number', '3');
while (!num2 || !isFinite(num2)) {
    num2 = +prompt('Number is expected, Please re-enter');
}

function math(num1, sign, num2) {
    switch (sign) {
        case "+":
            return (num1 + num2);
        case "-":
            return (num1 - num2);
        case "*":
            return (num1 * num2);
        case "/":
            return (num1 / num2);
        default: alert("Something wrong");
    }
}

console.log(math(num1, sign, num2));



